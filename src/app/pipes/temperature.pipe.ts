import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temperature'
})
export class TemperaturePipe implements PipeTransform {
  private readonly kelvin = 273.15;

  /**
   * Change temperature on celsius
   * @param value temperature in kelvin
   * @returns returns a number temperature in celsius
   */
  transform(value: number): number {
    return Math.round(value - this.kelvin);
  }
}
