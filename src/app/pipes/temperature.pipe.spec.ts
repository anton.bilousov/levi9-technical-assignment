import { TemperaturePipe } from './temperature.pipe';

describe('TemperaturePipe', () => {
  it('create an instance', () => {
    const pipe = new TemperaturePipe();
    expect(pipe).toBeTruthy();
  });
  it('should return celsius', () => {
    const pipe = new TemperaturePipe();
    expect(pipe.transform(273.15)).toEqual(0);
  });
});
