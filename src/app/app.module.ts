import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatSliderModule } from '@angular/material/slider';
import {MatSelectModule} from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChooseCityComponent } from './choose-city/choose-city.component';
import { CityInformationComponent } from './city-information/city-information.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { TemperaturePipe } from './pipes/temperature.pipe';
import { TemperatureComponent } from './temperature/temperature.component';
import { WeatherConditionsComponent } from './weather-conditions/weather-conditions.component';
import {StoreModule} from '@ngrx/store';
import {appReducer} from './store/reducers/app.reducer';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { HourlyInformationComponent } from './hourly-information/hourly-information.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { WindComponent } from './wind/wind.component';

@NgModule({
  declarations: [
    AppComponent,
    ChooseCityComponent,
    CityInformationComponent,
    TemperaturePipe,
    TemperatureComponent,
    WeatherConditionsComponent,
    HourlyInformationComponent,
    WindComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSelectModule,
    StoreModule.forRoot(appReducer),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    HttpClientModule,
    NoopAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
