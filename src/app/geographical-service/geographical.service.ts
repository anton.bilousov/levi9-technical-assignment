import { Injectable } from '@angular/core';
import {GeoNamesInterface, IGeo} from '../city-information/city-information.interfaces';

@Injectable({
  providedIn: 'root'
})
export class GeographicalService {
  private readonly geo: Array<IGeo> = [{
    name: 'Kyiv',
    lat: 50.433,
    lon: 30.517,
    timezone: '+2'
  }, {
    name: 'London',
    lat: 51.508,
    lon:  -0.126,
    timezone: '+0'
  }, {
    name: 'Stockholm',
    lat: 59.333,
    lon: 18.065,
    timezone: '+1'
  }, {
    name: 'Amsterdam',
    lat: 52.374,
    lon: 4.890,
    timezone: '+1'
  }, {
    name: 'Berlin',
    lat: 52.524,
    lon: 13.411,
    timezone: '+1'
  }];

  get cities(): Array<string> {
    return this.geo.map((city) => city.name);
  }

  /**
   * This is the foo function
   * @param country City name
   * @returns return an city geographic information
   */
  getLocation(country: string): any {
    return this.geo.find((con: IGeo) => con.name === country);
  }
}
