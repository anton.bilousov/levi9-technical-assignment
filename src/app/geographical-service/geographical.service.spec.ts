import { TestBed } from '@angular/core/testing';

import { GeographicalService } from './geographical.service';

describe('GeographicalService', () => {
  let service: GeographicalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeographicalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return value', () => {
    const x = {
      lat: 50.433,
      lon: 30.517,
      name: 'Kyiv',
      timezone: '+2'
    };
    expect(service.getLocation('Kyiv')).toEqual(x);
  });
});
