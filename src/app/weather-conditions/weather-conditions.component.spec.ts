import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherConditionsComponent } from './weather-conditions.component';

describe('WeatherConditionsComponent', () => {
  let component: WeatherConditionsComponent;
  let fixture: ComponentFixture<WeatherConditionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherConditionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return image url', () => {
    const testIcon = `http://openweathermap.org/img/wn/test@2x.png`;
    component.weatherConditions = {icon: 'test', main: '', description: ''};
    expect(component.getIcon).toEqual(testIcon);
  });
});
