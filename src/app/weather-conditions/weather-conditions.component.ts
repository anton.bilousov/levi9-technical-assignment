import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {WeatherConditionsInterface} from './weather-conditions.interface';

@Component({
  selector: 'app-weather-conditions',
  templateUrl: './weather-conditions.component.html',
  styleUrls: ['./weather-conditions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherConditionsComponent {
  @Input() weatherConditions: WeatherConditionsInterface | undefined;

  get getIcon(): string {
    return this.weatherConditions?.icon
      ? `http://openweathermap.org/img/wn/${this.weatherConditions.icon}@2x.png`
      : '';
  }
}
