export interface WeatherConditionsInterface {
  icon?: string;
  main: string;
  description: string;
}
