import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChooseCityComponent } from './choose-city.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

describe('ChooseCityComponent', () => {
  let component: ChooseCityComponent;
  let fixture: ComponentFixture<ChooseCityComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule.withRoutes([])],
      declarations: [ ChooseCityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect on submit', () => {
    const name = component.form.controls['orders'];
    const navigateSpy = spyOn(router, 'navigate');
    name.setValue('Kyiv');
    component.submit();
    expect(navigateSpy).toHaveBeenCalledWith(['Kyiv']);
  });
});
