import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {GeographicalService} from '../geographical-service/geographical.service';

@Component({
  selector: 'app-choose-city',
  templateUrl: './choose-city.component.html',
  styleUrls: ['./choose-city.component.scss']
})
export class ChooseCityComponent {
  public readonly cities = this.geoService.cities;
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private geoService: GeographicalService
  ) {
    this.form = this.formBuilder.group({
      orders: ['']
    });
  }

  submit(): void {
    this.router.navigate([this.form.get('orders')?.value]);
  }
}
