export interface IWeatherState {
  weather: Array<Interface>;
}

export interface Interface {
  main: string;
  description: string;
}

export const initialWeatherState: IWeatherState = {
  weather: [{
    main: '',
    description: ''
  }]
};
