export interface IWindState {
  wind: IWind;
}

export interface IWind {
  deg: number;
  speed: number;
}

export const initialWindState: IWindState  = {
  wind: {
    deg: 0,
    speed: 0
  }
};
