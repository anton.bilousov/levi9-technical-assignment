import {initialWeatherState, IWeatherState} from './weather.state';
import {initialMainState, ITemperatureState} from './main.state';
import {IHourlyState, initialHourlyState} from './hourly.state';
import {initialWindState, IWindState} from './wind.state';

export interface IAppState {
  main: ITemperatureState;
  weather: IWeatherState;
  hourly: IHourlyState;
  wind: IWindState;
}

export const initialAppState: IAppState = {
  main: initialMainState,
  weather: initialWeatherState,
  hourly: initialHourlyState,
  wind: initialWindState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
