import {ITemperature} from '../../temperature/temperature.interface';

export interface ITemperatureState {
  main: ITemperature;
}

export const initialMainState: ITemperatureState = {
  main: {
    temp: 0,
    temp_max: 0,
    temp_min: 0,
    pressure: 0,
    humidity: 0,
    feels_like: 0
  }
};
