import {ITemperature} from '../../temperature/temperature.interface';
import {HourlyInformationInterfaces} from '../../hourly-information/hourly-information.interfaces';

export interface IHourlyState {
  hourly: Array<HourlyInformationInterfaces>;
}

export interface InterfaceHourly {
  weather: Array<any>;
  main: ITemperature;
}

export const initialHourlyState: IHourlyState = {
  hourly: []
};
