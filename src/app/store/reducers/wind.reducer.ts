import {initialWindState} from '../state/wind.state';
import {EWindActions, WindActions} from '../actions/wind.action';


export function windReducer(state = initialWindState, action: WindActions) {
  switch (action.type) {
    case EWindActions.GetWind:
      return {...state, wind: action.payload};
    default:
      return state;
  }
}
