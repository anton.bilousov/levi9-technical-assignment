import {initialMainState} from '../state/main.state';
import {EMetricActions, MetricActions} from '../actions/main.action';


export function metricReducer(state = initialMainState, action: MetricActions) {
  switch (action.type) {
    case EMetricActions.GetMetrics:
      return {...state, main: action.payload};
    default:
      return state;
  }
}
