import {initialWeatherState} from '../state/weather.state';
import {EWeatherActions, WeatherActions} from '../actions/weather.action';


export function weatherReducer(state = initialWeatherState, action: WeatherActions) {
  switch (action.type) {
    case EWeatherActions.GetWeather:
      return {...state, weather: action.payload};
    default:
      return state;
  }
}
