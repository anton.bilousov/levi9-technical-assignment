import { ActionReducerMap } from '@ngrx/store';
import {metricReducer} from './main.reducer';
import {weatherReducer} from './weather.reducer';
import {IAppState} from '../state/app.state';
import {hourlyReducer} from './hourly.reducer';
import {windReducer} from './wind.reducer';

export const appReducer: ActionReducerMap<IAppState, any> = {
  main: metricReducer,
  weather: weatherReducer,
  hourly: hourlyReducer,
  wind: windReducer
};
