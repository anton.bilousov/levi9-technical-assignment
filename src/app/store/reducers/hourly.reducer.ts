import {initialHourlyState} from '../state/hourly.state';
import {EHourlyActions, HourlyActions} from '../actions/hourly.action';

export function hourlyReducer(state = initialHourlyState, action: HourlyActions) {
  switch (action.type) {
    case EHourlyActions.GetHourly:
      console.log(action.payload);
      return {...state, hourly: action.payload};
    case EHourlyActions.ClearHourly:
      return {...state, hourly: []};
    default:
      return state;
  }
}
