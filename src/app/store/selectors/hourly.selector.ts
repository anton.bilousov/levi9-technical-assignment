import {createSelector} from '@ngrx/store';
import {IAppState} from '../state/app.state';
import {IHourlyState} from '../state/hourly.state';


const WeatherInState = (state: IAppState) => state.hourly;

export const selectHourly = createSelector(
  WeatherInState,
  (state: IHourlyState) => state.hourly
);
