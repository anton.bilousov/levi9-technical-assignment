import {createSelector} from '@ngrx/store';
import {IAppState} from '../state/app.state';
import {IWeatherState} from '../state/weather.state';


const WeatherInState = (state: IAppState) => state.weather;

export const selectWeather = createSelector(
  WeatherInState,
  (state: IWeatherState) => state.weather
);
