import {createSelector} from '@ngrx/store';
import {IAppState} from '../state/app.state';


const metricInState = (state: IAppState) => state.main;

export const selectMain = createSelector(
  metricInState,
  (state: any) => state.main
);
