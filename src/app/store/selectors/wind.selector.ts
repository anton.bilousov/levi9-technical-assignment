import {createSelector} from '@ngrx/store';
import {IAppState} from '../state/app.state';


const windInState = (state: IAppState) => state.wind;

export const selectWind = createSelector(
  windInState,
  (state: any) => state.wind
);
