import {Action} from '@ngrx/store';


export enum EWindActions {
  GetWind = 'Get wind'
}

export class GetWind implements Action {
  public readonly type = EWindActions.GetWind;
  constructor(public payload: any) {}
}

export type WindActions = GetWind;
