import {Action} from '@ngrx/store';


export enum EWeatherActions {
  GetWeather = 'Get weather'
}

export class GetWeather implements Action {
  public readonly type = EWeatherActions.GetWeather;
  constructor(public payload: any) {}
}

export type WeatherActions = GetWeather;
