import {Action} from '@ngrx/store';


export enum EMetricActions {
  GetMetrics = 'Get metrics'
}

export class GetMetric implements Action {
  public readonly type = EMetricActions.GetMetrics;
  constructor(public payload: any) {}
}

export type MetricActions = GetMetric;
