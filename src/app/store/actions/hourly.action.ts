import {Action} from '@ngrx/store';


export enum EHourlyActions {
  GetHourly = 'Get Hourly',
  ClearHourly = 'Clear Hourly'
}

export class ClearHourly implements Action {
  public readonly type = EHourlyActions.ClearHourly;
}

export class GetHourly implements Action {
  public readonly type = EHourlyActions.GetHourly;
  constructor(public payload: any) {}
}

export type HourlyActions = GetHourly | ClearHourly;
