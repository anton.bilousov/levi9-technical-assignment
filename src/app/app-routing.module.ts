import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CityInformationComponent} from './city-information/city-information.component';

const routes: Routes = [{
  path: ':cityId',
  component: CityInformationComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
