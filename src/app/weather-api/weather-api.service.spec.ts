import { TestBed } from '@angular/core/testing';
import { WeatherApiService } from './weather-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WeatherApiService', () => {
  let service: WeatherApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(WeatherApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
