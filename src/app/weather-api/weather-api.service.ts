import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IGeo} from '../city-information/city-information.interfaces';

@Injectable({
  providedIn: 'root'
})
export class WeatherApiService {
  private readonly apiKey = 'bef15c5b1c168b9a0a8bc70cd735bee1';
  private readonly hostName = 'http://api.openweathermap.org/data/2.5';

  constructor(private http: HttpClient) {}

  getData(city: string): Observable<object> {
    let params = new HttpParams();
    params = params.append('q', city);
    params = params.append('appid', this.apiKey);
    return this.http.get(`${this.hostName}/weather`, {params});
  }

  getDataHourly(coordinates: IGeo): Observable<object> {
    let params = new HttpParams();
    params = params.append('lat', `${coordinates.lat}`);
    params = params.append('lon', `${coordinates.lon}`);
    params = params.append('exclude', 'current,minutely,daily');
    params = params.append('appid', this.apiKey);
    return this.http.get(
      `${this.hostName}/onecall`,
      {params}
    );
  }
}
