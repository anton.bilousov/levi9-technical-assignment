import {WeatherConditionsInterface} from '../weather-conditions/weather-conditions.interface';
import {ITemperature} from '../temperature/temperature.interface';

export interface HourlyInformationInterfaces extends ITemperature{
  weather: Array<WeatherConditionsInterface>;
  wind_deg: number;
  wind_speed: number;
  dt: number;
}
