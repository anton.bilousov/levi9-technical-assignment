import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IWind} from '../store/state/wind.state';
import {WeatherConditionsInterface} from '../weather-conditions/weather-conditions.interface';
import {HourlyInformationInterfaces} from './hourly-information.interfaces';

@Component({
  selector: 'app-hourly-information',
  templateUrl: './hourly-information.component.html',
  styleUrls: ['./hourly-information.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HourlyInformationComponent {
  @Input() weatherInfo: HourlyInformationInterfaces;
  @Input() timezone: string;

  get timeInMilliseconds(): number {
    return (this.weatherInfo.dt * 1000);
  }

  get weatherCondition(): Array<WeatherConditionsInterface> {
    return this.weatherInfo.weather;
  }

  get wind(): IWind {
    return {deg: this.weatherInfo.wind_deg, speed: this.weatherInfo.wind_speed};
  }
}
