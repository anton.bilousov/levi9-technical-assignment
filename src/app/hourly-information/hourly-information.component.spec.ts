import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyInformationComponent } from './hourly-information.component';

const mock =  {
  weather: [{main: '', description: ''}],
  wind_deg: 0,
  wind_speed: 0,
  dt: 0,
  temp: 0,
  pressure: 0
};

describe('HourlyInformationComponent', () => {
  let component: HourlyInformationComponent;
  let fixture: ComponentFixture<HourlyInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HourlyInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyInformationComponent);
    component = fixture.componentInstance;
    component.weatherInfo = mock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should map wind information', () => {
    expect(component.wind).toEqual({deg: 0, speed: 0});
  });
});
