export interface ITemperature {
  temp: number;
  temp_max?: number;
  temp_min?: number;
  pressure: number;
  humidity?: number;
  feels_like?: number;
}
