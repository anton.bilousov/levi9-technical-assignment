import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {ITemperature} from './temperature.interface';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemperatureComponent {
  @Input() temperature: ITemperature;
}
