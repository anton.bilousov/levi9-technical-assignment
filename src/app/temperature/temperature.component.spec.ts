import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemperatureComponent } from './temperature.component';
import {TemperaturePipe} from '../pipes/temperature.pipe';

describe('TemperatureComponent', () => {
  let component: TemperatureComponent;
  let fixture: ComponentFixture<TemperatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemperatureComponent, TemperaturePipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemperatureComponent);
    component = fixture.componentInstance;
    component.temperature = {temp: 0, pressure: 1000};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
