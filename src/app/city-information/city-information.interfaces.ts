export interface IGeo extends GeoNamesInterface{
  lat: number;
  lon: number;
  timezone: string;
}

export interface GeoNamesInterface {
  name: string;
}
