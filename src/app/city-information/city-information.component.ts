import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WeatherApiService} from '../weather-api/weather-api.service';
import {select, Store} from '@ngrx/store';
import {GetMetric} from '../store/actions/main.action';
import {selectMain} from '../store/selectors/main.selector';
import {GetWeather} from '../store/actions/weather.action';
import {selectWeather} from '../store/selectors/weather.reducer';
import {GeographicalService} from '../geographical-service/geographical.service';
import {ClearHourly, GetHourly} from '../store/actions/hourly.action';
import {selectHourly} from '../store/selectors/hourly.selector';
import {GetWind} from '../store/actions/wind.action';
import {selectWind} from '../store/selectors/wind.selector';
import {IGeo} from './city-information.interfaces';
import {IAppState} from '../store/state/app.state';
import {IHourlyState} from '../store/state/hourly.state';


@Component({
  selector: 'app-city-information',
  templateUrl: './city-information.component.html',
  styleUrls: ['./city-information.component.scss']
})
export class CityInformationComponent implements OnInit {
  public city: string;
  public cityInfo$ = this.store.pipe(select(selectWeather));
  public tempr$ = this.store.pipe(select(selectMain));
  public hourlyInformation$ = this.store.pipe(select(selectHourly));
  public wind$ = this.store.pipe(select(selectWind));

  get geoLocation(): IGeo {
    return this.geo.getLocation(this.city);
  }

  get timeZone(): string {
    return this.geo.getLocation(this.city).timezone;
  }

  constructor(
    private route: ActivatedRoute,
    private service: WeatherApiService,
    private store: Store<IAppState>,
    private geo: GeographicalService
  ) {}
  /**
   * Get all weather information by the city name and dispatch it in the store
   */
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.city = params.get('cityId') || '';

      if (this.geo.getLocation(this.city)) {
        this.service.getData(this.city).subscribe((r) => {
          // clear previous information
          this.store.dispatch(new ClearHourly());
          this.dispatchData((r as IAppState));
        });
      }
    });
  }

  /**
   * Add weather information in the store
   * @param openweather weather information
   */
  dispatchData(openweather: IAppState): void {
    this.store.dispatch(new GetMetric(openweather.main));
    this.store.dispatch(new GetWeather(openweather.weather));
    this.store.dispatch(new GetWind(openweather.wind));
  }
  /**
   * Get hourly weather information and dispatch it in the store
   */
  more(): void {
    this.service.getDataHourly(this.geoLocation)
      .subscribe(elm => {
        this.store.dispatch(new GetHourly((elm as IHourlyState).hourly.slice(1, 8)));
    });
  }
}
