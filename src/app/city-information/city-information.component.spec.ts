import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CityInformationComponent } from './city-information.component';
import {RouterModule} from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Store, StoreModule} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';

const mock = { weather: {weather: [{
      id: 800,
      main: 'Clear',
      description: 'clear sky',
      icon: '01d'}]}, main: {}, wind: {}, hourly: {}};

describe('CityInformationComponent', () => {
  let component: CityInformationComponent;
  let fixture: ComponentFixture<CityInformationComponent>;
  let store;

  beforeEach(async () => {
    const initialState = mock;

    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), HttpClientTestingModule, StoreModule.forRoot({})],
      declarations: [ CityInformationComponent ],
      providers: [
        provideMockStore({ initialState })
      ],
    })
    .compileComponents();

    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return timezone', () => {
    component.city = 'Kyiv';
    expect(component.timeZone).toEqual('+2');
  });
});
