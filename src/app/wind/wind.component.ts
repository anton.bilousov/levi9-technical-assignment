import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IWind} from '../store/state/wind.state';

@Component({
  selector: 'app-wind',
  templateUrl: './wind.component.html',
  styleUrls: ['./wind.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WindComponent {
  @Input() wind?: IWind;
}
